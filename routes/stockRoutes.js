import stockController from '../controllers/stockController';
import express from 'express';

// Creating router instance of express router 
const router = express.Router();

// Route to get stock data
router.route('/stock/:symbol')
   .get(
      stockController.getSectors
   );
  




module.exports = router;