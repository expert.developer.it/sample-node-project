import stockModel from '../models/stockModel';
import request from 'request';
import yahooFinance from 'yahoo-finance';

module.exports = {
  getSectors(req, res, next) {
    let symbol = req.params.symbol.toUpperCase();
    if (symbol) {
      yahooFinance.quote({
        symbol: symbol,
        modules: ['summaryProfile']
      }, function(err, quotes) {
        if (err) {
          res.status(500).json({
            message: err.message
          })
        } else {
          let data = {
            symbol,
            industry: quotes.summaryProfile.industry,
            sector: quotes.summaryProfile.sector
          };

          res.status(200).json({
            message: "Summary For The Stock Symbol",
            data
          })
        }
      });
    } else {
      res.status(400).json({
        message: "symbol is required"
      })
    }
  }
}