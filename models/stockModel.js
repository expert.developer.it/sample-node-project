import mongoose from 'mongoose';

const stockSchema = new mongoose.Schema({

  sumbol: {
    type: String,
    default: ""
  },
 
}, {
  timestamps: true
});

const stock = mongoose.model('stock', stockSchema);

module.exports = stock;